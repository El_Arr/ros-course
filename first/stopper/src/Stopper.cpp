#include "Stopper.h"
#include "geometry_msgs/Twist.h"
#include <ros/console.h>

Stopper::Stopper() {
	isRotating = false;
	commandPub = node.advertise<geometry_msgs::Twist>("/cmd_vel", 10);
	laserSub = node.subscribe("scan", 1, &Stopper::scanCallback, this);
}

void Stopper::moveForward() {
	if (isRotating) return;
	geometry_msgs::Twist msg;
	msg.linear.x = (-1) * FORWARD_SPEED;
	commandPub.publish(msg);
}

void Stopper::scanCallback(const sensor_msgs::LaserScan::ConstPtr &scan) {
	bool isObstacleInFront = false;
	int minIndex, maxIndex;

	minIndex = ceil((MIN_SCAN_ANGLE - scan->angle_min) / scan->angle_increment);
	maxIndex = floor((MAX_SCAN_ANGLE - scan->angle_min) / scan->angle_increment);
	for (int currIndex = minIndex + 1; currIndex <= maxIndex; currIndex++) {
		if (scan->ranges[currIndex] < MIN_DIST_FROM_OBSTACLE) {
			isObstacleInFront = true;
			break;
		}
	}

	if (isObstacleInFront && !isRotating) {
		isRotating = true;
		minIndex = ceil(scan->angle_min / scan->angle_increment);
		maxIndex = floor(scan->angle_max / scan->angle_increment);
		int maxRange = 0;
		int index = 0;
		for (int currIndex = minIndex + 1; currIndex <= maxIndex; currIndex++) {
			if (scan->range_min <= scan->ranges[currIndex] && scan->ranges[currIndex] <= scan->range_max) {
				if (scan->ranges[currIndex] > maxRange) {
					maxRange = scan->ranges[currIndex];
					index = currIndex;
				}
			}
		}
		int targetAngle = index * scan->angle_increment;

		geometry_msgs::Twist msg;
		if (targetAngle < M_PI) {
			targetAngle = M_PI - targetAngle;
			msg.angular.z = -ANGULAR_SPEED;
		} else {
			targetAngle -= M_PI;
			msg.angular.z = ANGULAR_SPEED;
		}
		int time = targetAngle / ANGULAR_SPEED;
		commandPub.publish(msg);
		ros::Duration(time).sleep();
	}

	if (!isObstacleInFront && isRotating) {
		geometry_msgs::Twist msg;
		commandPub.publish(msg);
		isRotating = false;
	}
}