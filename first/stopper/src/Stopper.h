#ifndef STOPPER_H
#define STOPPER_H

#include "ros/ros.h"
#include "sensor_msgs/LaserScan.h"

class Stopper {
public:
	static constexpr double FORWARD_SPEED = 0.2;
	static constexpr double ANGULAR_SPEED = 0.6;
	static constexpr double ANGLE = 20.0;
	static constexpr double MIN_SCAN_ANGLE = M_PI - (ANGLE / 180 * M_PI);
	static constexpr double MAX_SCAN_ANGLE = M_PI + (ANGLE / 180 * M_PI);
	static constexpr float MIN_DIST_FROM_OBSTACLE = 0.6f;

	Stopper();

	void moveForward();

private:
	ros::NodeHandle node;
	ros::Publisher commandPub;
	ros::Subscriber laserSub;

	bool isRotating;

	void scanCallback(const sensor_msgs::LaserScan::ConstPtr &scan);
};

#endif // STOPPER_H