#include "ros/ros.h"
#include "mapping/CleanMap.h"
#include <cstdint>
#include <set>
#include <queue>

using namespace std;

bool same(signed char a, signed char b) {
	return (a == 0 && b == 0) || (a == 100 && b == 100) || (a == -1 && b == -1);
}

bool clean(mapping::CleanMap::Request &req, mapping::CleanMap::Response &res) {
	int removed = 0;
	int vis = 0;
	int removed_pix = 0;
	int total = 0;
	signed char *data = req.map.data.data();
	int rows = req.map.info.height, cols = req.map.info.width;
	for (int pass = 0; pass < 3; ++pass) {
		ROS_INFO("Pass %d", pass + 1);

		set<int> visited;
		set<int> cluster;
		queue<int> cells;
		int currCell = 0;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				// If the cell is not visited
				if (visited.find(currCell) == visited.end()) {
					cells.push(currCell);
					cluster.insert(currCell);
					while (!cells.empty()) {
						int cell = cells.front();
						cells.pop();
						// If the cell is visited
						if (visited.find(cell) != visited.end()) continue;
						visited.insert(cell);
						set<int> neighbors;
						// If it is not the first column, get left neighbor
						if ((cell % cols) != 0) neighbors.insert(cell - 1);
						// If it is not the last column, get right neighbor
						if (((cell + 1) % cols) != 0) neighbors.insert(cell + 1);
						// If it is not the first row, get upper neighbor
						if (cell >= cols) neighbors.insert(cell - cols);
						// If it is not the last row, get lower neighbor
						if (cell <= ((rows - 1) * cols - 1)) neighbors.insert(cell + cols);

						for (int n : neighbors) {
							if (visited.find(n) == visited.end() && same(data[cell], data[n])) {
								cluster.insert(n);
								cells.push(n);
							}
						}
					}
					if (cluster.size() < 4) {
						removed++;
						for (int k : cluster) {
							removed_pix++;
							if (data[k] == 0) data[k] = 100;
							else data[k] = 0;
						}
					}
					total++;
					cluster.clear();
				}
				currCell++;
			}
		}
		vis = visited.size();
	}

	res.map = req.map;
	ROS_INFO("Visited %d cells, Total clusters %d", vis, total);
	ROS_INFO("Map cleaned, removed %d clusters (%d pixels in total)", removed, removed_pix);
	return true;
}

int main(int argc, char **argv) {
	ros::init(argc, argv, "map_service");
	ros::NodeHandle n;

	ros::ServiceServer service = n.advertiseService("clean_map", clean);
	ros::spin();

	return 0;
}