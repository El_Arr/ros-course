#include "ros/ros.h"
#include "std_msgs/String.h"
#include <ctime>
#include <sstream>

int main(int argc, char **argv) {
	ros::init(argc, argv, "timer");
	ros::NodeHandle n;
	ros::Publisher timer_pub = n.advertise<std_msgs::String>("timer", 10);
	ros::Rate loop_rate(2);
	time_t time;
	tm *time_info;
	while (ros::ok()) {
		time = std::time(nullptr);
		time_info = std::localtime(&time);
		std_msgs::String msg;
		std::stringstream ss;
		ss << time_info->tm_hour << ":" << time_info->tm_min << " "
		   << time_info->tm_mday << "." << 1 + time_info->tm_mon << "." << 1900 + time_info->tm_year;
		msg.data = ss.str();
		ROS_INFO("%s", msg.data.c_str());
		timer_pub.publish(msg);
		ros::spinOnce();
		loop_rate.sleep();
	}
	return 0;
}