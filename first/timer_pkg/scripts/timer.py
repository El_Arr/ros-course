#!/usr/bin/env python

import datetime

import rospy
from std_msgs.msg import String


class Timer:
	def __init__(self):
		pub = rospy.Publisher('time', String, queue_size=10)
		rospy.init_node('timer', anonymous=True)
		rate = rospy.Rate(2)
		while not rospy.is_shutdown():
			now = datetime.datetime.now()
			pub_str = now.strftime("%H:%M %d.%m.%Y")
			rospy.loginfo(pub_str)
			pub.publish(pub_str)
			rate.sleep()


if __name__ == '__main__':
	try:
		Timer()
	except rospy.ROSInterruptException:
		pass