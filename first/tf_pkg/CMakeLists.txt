cmake_minimum_required(VERSION 3.0.2)
project(tf_pkg)

find_package(
		catkin REQUIRED COMPONENTS
		roscpp
		rospy
		tf
		turtlesim
)

catkin_package(
		CATKIN_DEPENDS
		roscpp
		rospy
		tf
		turtlesim
)

include_directories(
		${catkin_INCLUDE_DIRS}
)

add_executable(
		robot_location
		src/robot_location.cpp
)
add_executable(
		tf_broadcaster
		src/tf_broadcaster.cpp
)
add_executable(
		tf_listener
		src/tf_listener.cpp
)

target_link_libraries(
		robot_location
		${catkin_LIBRARIES}
)
target_link_libraries(
		tf_broadcaster
		${catkin_LIBRARIES}
)
target_link_libraries(
		tf_listener
		${catkin_LIBRARIES}
)

catkin_install_python(
		PROGRAMS
		scripts/robot_location.py
		scripts/tf_broadcaster.py
		scripts/tf_listener.py
		DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(
		FILES
		launch/map_loader.cpp.launch
		launch/map_loader.py.launch
		launch/tf.2.cpp.launch
		launch/tf.2.py.launch
		launch/tf.cpp.launch
		launch/tf.py.launch
		DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)