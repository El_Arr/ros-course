#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "turtlesim/Pose.h"
#include <iostream>

using namespace std;

struct Turtle {
	std::vector<double> initial_pos = {0, 0, 0};
	std::vector<double> current_pos = {0, 0, 0};

	Turtle(int argc, char **argv) {
		const int LINEAR_SPEED = 1;
		string robot_name = string(argv[1]);
		int robot_distance = stoi(argv[2]);
		double robot_rotation = ((stoi(argv[3]) % 360) * M_PI / 360);

		ros::init(argc, argv, "turtle_node");
		ros::NodeHandle node;

		ros::Publisher pub = node.advertise<geometry_msgs::Twist>(robot_name + "/cmd_vel", 10);
		ros::Subscriber sub = node.subscribe(robot_name + "/pose", 10, &Turtle::poseCallback, this);

		ros::Rate rate(10);
		while (ros::ok() && abs(current_pos[0] - initial_pos[0]) < robot_distance) {
			geometry_msgs::Twist msg;
			msg.linear.x = LINEAR_SPEED;
			pub.publish(msg);
			ros::spinOnce();
			rate.sleep();
		}
		while (ros::ok() && round(abs(current_pos[2] - initial_pos[2]) * 10) / 10 <
		                    round(robot_rotation * 10) / 10) {
			ROS_INFO("current: %.2f, initial: %.2f, rotation: %.2f", current_pos[2], initial_pos[2],
			         robot_rotation);
			geometry_msgs::Twist msg;
			msg.angular.z = robot_rotation;
			pub.publish(msg);
			ros::spinOnce();
			rate.sleep();
		}
	}

	void poseCallback(const turtlesim::PoseConstPtr &msg) {
		ROS_INFO("x: %.2f, y: %.2f, theta: %.2f", msg->x, msg->y, msg->theta);
		if (initial_pos[0] == 0 && initial_pos[1] == 0) {
			initial_pos[0] = msg->x;
			initial_pos[1] = msg->y;
			initial_pos[2] = msg->theta;
		}
		current_pos[0] = msg->x;
		current_pos[1] = msg->y;
		current_pos[2] = msg->theta;
	}
};


int main(int argc, char **argv) {
	Turtle turtle(argc, argv);
	return 0;
}