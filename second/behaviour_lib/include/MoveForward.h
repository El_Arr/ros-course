#ifndef MOVEFORWARD_H
#define MOVEFORWARD_H

#include "Behavior.h"
#include <ros/ros.h>
#include "sensor_msgs/LaserScan.h"
#include <cmath>

class MoveForward : public Behavior {
public:
    MoveForward();

    virtual bool startCond();

    virtual void action();

    virtual bool stopCond();

    virtual ~MoveForward();

private:
    constexpr static double FORWARD_SPEED_MPS = 0.2;
    constexpr static double MIN_SCAN_ANGLE = M_PI + (-30.0 / 180 * M_PI);
    constexpr static double MAX_SCAN_ANGLE = M_PI + (+30.0 / 180 * M_PI);
    constexpr static float MIN_PROXIMITY_RANGE_M = 0.6;

    ros::NodeHandle node;
    ros::Publisher commandPub;
    ros::Subscriber laserSub;

    void scanCallback(const sensor_msgs::LaserScan::ConstPtr &scan);

    bool keepMovingForward;
};

#endif // MOVEFORWARD_H
