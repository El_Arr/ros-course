#include "ObstacleAvoidPlan.h"
#include "MoveForward.h"
#include "Rotate.h"
#include "TurnLeft.h"
#include "TurnRight.h"

ObstacleAvoidPlan::ObstacleAvoidPlan() {
    // Creating behaviors
    behaviors.push_back(new MoveForward());
	behaviors.push_back(new Rotate());
    behaviors.push_back(new TurnLeft());
    behaviors.push_back(new TurnRight());

    // Connecting behaviors
    behaviors[0]->addNext(behaviors[1]);

	behaviors[1]->addNext(behaviors[2]);
	behaviors[1]->addNext(behaviors[3]);

    behaviors[2]->addNext(behaviors[0]);
    behaviors[3]->addNext(behaviors[0]);

    startBehavior = behaviors[0];
}

ObstacleAvoidPlan::~ObstacleAvoidPlan() {

}
