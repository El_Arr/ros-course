#include "TurnRight.h"
#include "geometry_msgs/Twist.h"

TurnRight::TurnRight() {
    commandPub = node.advertise<geometry_msgs::Twist>("cmd_vel", 10);
    laserSub = node.subscribe("scan", 1, &TurnRight::scanCallback, this);
    keepTurningRight = true;
}

bool TurnRight::startCond() {
    return keepTurningRight;
}

void TurnRight::action() {
    geometry_msgs::Twist msg;
    if (keepTurningRight) {
        msg.angular.z = -TURN_SPEED_MPS;
        ROS_INFO("Turning right");
    } else {
        msg.angular.z = 0;
        ROS_INFO("Stop turning right");
    }
    commandPub.publish(msg);
}

bool TurnRight::stopCond() {
    return !keepTurningRight;
}

void TurnRight::scanCallback(const sensor_msgs::LaserScan::ConstPtr &scan) {
    // Find the closest range between the defined minimum and maximum angles
    int minIndex = ceil((MIN_SCAN_ANGLE - scan->angle_min) / scan->angle_increment);
    int maxIndex = floor((MAX_SCAN_ANGLE - scan->angle_min) / scan->angle_increment);

    float closestRange = scan->ranges[minIndex];
    for (int currIndex = minIndex + 1; currIndex <= maxIndex; currIndex++) {
        if (scan->ranges[currIndex] < closestRange) {
            closestRange = scan->ranges[currIndex];
        }
    }

    if (closestRange < MIN_PROXIMITY_RANGE_M) {
        keepTurningRight = true;
    } else {
        keepTurningRight = false;
    }
}

TurnRight::~TurnRight() {
}
