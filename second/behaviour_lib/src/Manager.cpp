#include "Manager.h"
#include <ros/ros.h>

Manager::Manager(Plan *plan) : plan(plan) {
    currBehavior = plan->getStartBehavior();
}

void Manager::run() {
    ros::Rate rate(10);
    if (!currBehavior->startCond()) {
        ROS_ERROR("Cannot start the first behavior");
        return;
    }
    while (ros::ok() && currBehavior != NULL) {
        currBehavior->action();

        if (currBehavior->stopCond()) {
            currBehavior = currBehavior->selectNext();
        }
        ros::spinOnce();
        rate.sleep();
    }
    ROS_INFO("Manager stopped");
}

Manager::~Manager() {

}
