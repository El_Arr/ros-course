cmake_minimum_required(VERSION 3.0.2)
project(pioneer)

find_package(
        catkin REQUIRED COMPONENTS
        joint_state_publisher
        joint_state_publisher_gui
        urdf
        xacro
)

catkin_package()

include_directories(${catkin_INCLUDE_DIRS})