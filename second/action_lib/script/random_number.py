#! /usr/bin/env python

from __future__ import print_function
from rosgraph.names import load_mappings

import rospy

from std_msgs.msg import Float32
import random

if __name__ == '__main__':
    try:
        rospy.init_node('random_number')
        random_number_pub = rospy.Publisher("/random_number", Float32, queue_size=1)
        loop_rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            msg = Float32()
            msg.data = random.uniform(1.0, 100.0)
            random_number_pub.publish(msg)
            loop_rate.sleep()
    except rospy.ROSInterruptException:
        print("program interrupted before completion", file=sys.stderr)
