#include "dumpster.cpp"

int main(int argc, char **argv) {
	ros::init(argc, argv, "dumpster");
	Dumpster dumpster;
	dumpster.search();
	return 0;
}