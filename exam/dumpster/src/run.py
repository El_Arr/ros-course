#!/usr/bin/env python2
from __future__ import division

import random

import rospy
import sys
import cv2
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image, LaserScan
from geometry_msgs.msg import Twist
import numpy as np
import math

MIN_SCAN_ANGLE = math.pi - (5.0 / 180.0 * math.pi)
MAX_SCAN_ANGLE = math.pi + (5.0 / 180.0 * math.pi)





class image_converter:

    def __init__(self):

        self.large_side_ratio = 1.35
        self.small_side_ratio = 0.75
        self.eps = 0.15

        self.mode = 0  # 0 - Large Side, 1 - Small Side
        self.side_found = False
        self.ratio = 0.0

        self.keepMoving = True
        self.safe_distance = 1.0

        self.depth_image = None

        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("/camera/rgb/image_raw", Image, self.cb_image_receive)
        self.depth_image_sub = rospy.Subscriber("/camera/depth/image_raw", Image, self.cb_depth_image_receive)
        self.scan_sub = rospy.Subscriber('/scan', LaserScan, self.scan_clb)
        self.cmd_vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)

    def determine_container_side(self, img):
        max_horizontal = 0.0
        max_vertical = 0.0

        rows, cols = img.shape

        blurred = cv2.GaussianBlur(img, (15, 15), 2)
        kernel = np.ones((5, 5), np.uint8)
        dilation = cv2.dilate(blurred, kernel, iterations=1)

        # column-wise
        for i in range(0, cols):
            max_vertical = max(cv2.countNonZero(dilation[:, i]), max_vertical)

        # row-wise
        for i in range(0, rows):
            max_horizontal = max(cv2.countNonZero(dilation[i, :]), max_horizontal)

        ratio = max_horizontal / max_vertical

        # print("max_horizontal = %f" % max_horizontal)
        # print("max_vertical = %f" % max_vertical)
        # print("Ratio = %f" % ratio)
        return ratio

    def find_contour(self, binary_img):
        kernel = np.ones((5, 5), np.uint8)
        blurred = cv2.GaussianBlur(binary_img, (15, 15), 2)
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (15, 15))
        opened_mask = cv2.morphologyEx(blurred, cv2.MORPH_OPEN, kernel)
        masked_img = cv2.bitwise_and(binary_img, binary_img, mask=opened_mask)
        dilation = cv2.dilate(blurred, kernel, iterations=1)
        edged = cv2.Canny(dilation, 100, 200)
        _, contours, hierarchy = cv2.findContours(edged,
                                                  cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        if len(contours) == 0:
            return None

        max_cnt = max(contours, key=cv2.contourArea)

        return max_cnt

    def draw_bbox(self, binary_img, contour):
        # Get BBox
        x, y, w, h = cv2.boundingRect(contour)
        # Draw BBox
        cv2.rectangle(binary_img, (x, y), (x + w, y + h), (255, 0, 0), 2)

    def find_centroid(self, binary_img):
        M = cv2.moments(binary_img, True)
        cx = int(M['m10'] / M['m00'])
        cy = int(M['m01'] / M['m00'])
        return (cx, cy)

    def scan_clb(self, msg):
        min_index = int(round((MIN_SCAN_ANGLE - msg.angle_min) /
                              msg.angle_increment))
        max_index = int(round((MAX_SCAN_ANGLE - msg.angle_min) /
                              msg.angle_increment))
        curr_index = min_index + 1
        while curr_index <= max_index:
            if msg.ranges[int(curr_index)] < self.safe_distance:
                self.keepMoving = False
                break
            curr_index = curr_index + 1
        # distances = np.array(msg.ranges[min_index:max_index])
        # distances = distances[np.isfinite(distances)]
        # print(distances)
        # print(min(distances))
        # print(any(i < self.safe_distance for i in distances))
        # if len(distances):
        #    mind = min(distances)

    #     print(mind)
    #    if mind < self.safe_distance:
    #        self.keepMoving = False

    def cb_depth_image_receive(self, data):
        try:
            self.depth_image = self.bridge.imgmsg_to_cv2(data, desired_encoding='passthrough')
        except CvBridgeError as e:
            print(e)

        # self.cf_d = depth_image[self.cf_v, self.cf_u] / 1000.0

    def rotateTo(self, angle, clockwise):
        vel_msg = Twist()
        speed = 10  # [degrees/sec] speed
        if not clockwise:
            speed = speed * -1
        # Converting from angles to radians
        angular_speed = speed*3.14/180
        relative_angle = angle*3.14/180
        # define the Twist message
        vel_msg.linear.x = 0
        vel_msg.linear.y = 0
        vel_msg.linear.z = 0
        vel_msg.angular.x = 0
        vel_msg.angular.y = 0
        vel_msg.angular.z = angular_speed
        current_angle = 0
        start_time = rospy.get_rostime().secs
        while current_angle <= relative_angle and not rospy.is_shutdown():
            self.cmd_vel_pub.publish(vel_msg)
            current_angle = angular_speed * (rospy.get_rostime().secs - start_time)
        # stop a Roomba vacuum cleaner
        vel_msg.angular.z = 0
        self.cmd_vel_pub.publish(vel_msg)

    def cb_image_receive(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        cv_image = cv2.resize(cv_image, (640, 480))

        hsv_frame = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(hsv_frame, np.array([35, 50, 5]), np.array([80, 125, 55]))
        result = cv_image.copy()
        result = cv2.bitwise_and(result, result, mask=mask)
        result[mask > 0] = (255, 255, 255)

        if cv2.countNonZero(mask) == 0:
            print("Finding container...")
            cmd = Twist()
            cmd.linear.x = 0.1
            random_multiplier = 1 if random.random() < 0.5 else -1
            cmd.angular.z = random_multiplier * 0.5
            self.cmd_vel_pub.publish(cmd)
            return

        centroid = self.find_centroid(mask)

        # Analyzing side
        if(not self.side_found):
            print("Analyzing side...")
            scan_msg = rospy.wait_for_message("/scan", LaserScan, timeout=None)
            dists = [scan_msg.ranges[359], scan_msg.ranges[0]]
            dist = min(dists)
            cmd = Twist()
            if(dist < 1.75):
                cmd.linear.x = 0.0
                cmd.angular.z = 0.0
                self.cmd_vel_pub.publish(cmd)
                self.ratio = self.determine_container_side(mask)
                print("STOP!!!")
            else:
                err = centroid[0] - mask.shape[1] / 2
                cmd.linear.x = 0.05
                cmd.angular.z = -err / 300
                self.cmd_vel_pub.publish(cmd)
                print("Next analyze")
                return

        ratio = self.determine_container_side(mask)
        if ratio != self.ratio:
            print("Shift")
            self.side_found = False

        scan_msg = rospy.wait_for_message("/scan", LaserScan, timeout=None)
        # print("0 = %f" % scan_msg.ranges[0])
        # print("90 = %f" % scan_msg.ranges[90])
        # print("180 = %f" % scan_msg.ranges[180])
        # print("270 = %f" % scan_msg.ranges[270])
        # print("359 = %f" % scan_msg.ranges[359])
        ranges = []
        ranges.append(scan_msg.ranges[359])
        ranges.append(scan_msg.ranges[0])
        self.keepMoving = True
        if any(i < self.safe_distance for i in ranges):
            self.keepMoving = False

        print("Min distance = %f" % min(ranges))

        '''if(not self.side_found):
            print("Analyzing side...")
            cmd = Twist()
            while(True):
                img_msg = rospy.wait_for_message("/camera/rgb/image_raw", Image, timeout=None)
                cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
                cv_image = cv2.resize(cv_image, (640, 480))
                hsv_frame = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)
                mask = cv2.inRange(hsv_frame, np.array([35, 50, 5]), np.array([80, 125, 55]))
                centroid = self.find_centroid(mask)
                scan_msg = rospy.wait_for_message("/scan", LaserScan, timeout=None)
                dists = [scan_msg.ranges[359], scan_msg.ranges[0]]
                dist = min(dists)
                if(dist < 1.75):
                    cmd.linear.x = 0.0
                    cmd.angular.z = 0.0
                    self.cmd_vel_pub.publish(cmd)
                    break
                else:
                    err = centroid[0] - mask.shape[1] / 2
                    cmd.linear.x = 0.05
                    cmd.angular.z = -err / 300
                    self.cmd_vel_pub.publish(cmd)

            self.ratio = self.determine_container_side(mask)'''

        #self.ratio = self.determine_container_side(mask)

        side_contour = None
        msg = ""
        # if small side found
        if self.ratio < self.large_side_ratio:
            msg = "Small side found!"
            print(msg)
            if(self.mode == 0):
                print("But we need Large side...")
                self.rotateTo(90, True)
                self.side_found = False
                return
            else:
                self.side_found = True
            #print(ratio)
            side_contour = self.find_contour(mask)
            if side_contour is None:
                return

        # if large side found
        else:
            msg = "Large side found!"
            print(msg)
            if(self.mode == 1):
                print("But we need Small side...")
                self.rotateTo(90, True)
                self.side_found = False
                return
            else:
                self.side_found = True
            #print(ratio)
            side_contour = self.find_contour(mask)
            if side_contour is None:
                return

        self.draw_bbox(result, side_contour)
        cv2.putText(result, msg, (centroid[0], centroid[1]), cv2.FONT_HERSHEY_SIMPLEX, 1, 2)
        # print("X = %f, Y = %f" % (centroid[0], centroid[1]))
        cv2.circle(result, (centroid[0], centroid[1]), radius=0, color=(0, 0, 255), thickness=5)

        # Move robot
        cmd = Twist()
        if self.keepMoving:
            print("Moving forward!")
            err = centroid[0] - mask.shape[1] / 2
            cmd.linear.x = 0.05
            cmd.angular.z = -err / 300
            self.cmd_vel_pub.publish(cmd)
        else:
            print("Stop!")
            cmd.linear.x = 0.0
            cmd.angular.z = 0.0
            self.cmd_vel_pub.publish(cmd)

        cv2.imshow("main", result)
        # cv2.imshow("depth main", self.depth_image)
        cv2.waitKey(3)


def main(args):
    rospy.init_node('container_finder', anonymous=True)
    ic = image_converter()
    while not rospy.is_shutdown():
        try:
            rospy.spin()
        except KeyboardInterrupt as e:
            print("Shutting down")


if __name__ == '__main__':
    main(sys.argv)